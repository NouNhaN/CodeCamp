let peopleSalary = [
    {'id':'1001','firstname':'Luke','lastname':'Skywalker','company':'Walt Disney','salary':'40000'},
    {'id':'1002','firstname':'Tony','lastname':'Stark','company':'Marvel','salary':'1000000'},
    {'id':'1003','firstname':'Somchai','lastname':'Jaidee','company':'Love2work','salary':'20000'},
    {'id':'1004','firstname':'Monkey D','lastname':'Luffee','company':'One Piece','salary':'9000000'},
]
    
let peopleSalaryNew = [];

for (let i = 0; i < peopleSalary.length; i++) {
    let peopleSalaryText = peopleSalary[i];
    let peopleSalaryRecord = {};

    for (let x in peopleSalaryText) {
        if (x != 'company') {
            peopleSalaryRecord[x] = peopleSalaryText[x];
        }
    }

    peopleSalaryNew.push(peopleSalaryRecord);
}

let peopleSalaryNewArray = [];

for (let i = 0; i < peopleSalaryNew.length; i++) {
    let peopleSalaryNewText = peopleSalaryNew[i];
    let peopleSalaryNewRecord = {};

    for (let x in peopleSalaryNewText) {
        if (x == 'salary') {
            let salaryRecord = [];
            let salaryCal = Number(peopleSalaryNewText[x]);
          
            salaryRecord.push(salaryCal);
            for (let j = 0; j < 3; j++) {
                salaryCal = salaryCal + ((salaryCal * 10) / 100);
                salaryRecord.push(salaryCal);
            }
            peopleSalaryNewRecord[x] = salaryRecord;
        } else {
            peopleSalaryNewRecord[x] = peopleSalaryNewText[x];
        };
    }

    peopleSalaryNewArray.push(peopleSalaryNewRecord);
}
console.log(peopleSalaryNewArray); 
const fs = require('fs');
const os = require('os');

let dataHead = new Promise(function (resolve, reject) {
    fs.readFile('head.txt', 'utf8', function(err, data) {
        resolve(data);
    });                
});
let dataBody = new Promise(function (resolve, reject) {
    fs.readFile('body.txt', 'utf8', function(err, data) {
        resolve(data);
    });                
});
let dataLeg = new Promise(function (resolve, reject) {
    fs.readFile('leg.txt', 'utf8', function(err, data) {
        resolve(data);
    });                
});
let dataFeet = new Promise(function (resolve, reject) {
    fs.readFile('feet.txt', 'utf8', function(err, data) {
        resolve(data);
    });                
});

let robotData = '';

Promise.all([dataHead, dataBody, dataLeg, dataFeet])
.then(function(result){
    robotData = result[0] + os.EOL + result[1] + os.EOL + result[2] + os.EOL + result[3];
                        
    fs.writeFileSync('robot.txt', robotData, 'utf8');

    console.log('All completed!');
})
.catch(function(error){
    console.error("There's an error", error);
});
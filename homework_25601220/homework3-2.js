const fs = require('fs');
const os = require('os');

function read_dataFile(Filename) {
    return new Promise(function(resolve, reject) {
        fs.readFile(Filename, 'utf8', function(err, dataFile) {
            if (err)
                reject(err);
            else
                resolve(dataFile);
        });
    });
}

function write_dataFile(Filename, dataRobot) {
    return new Promise(function(resolve, reject) {
        fs.writeFile(Filename, dataRobot, 'utf8', function(err) {
            if (err)
                reject(err);
            else
                resolve();
        });
    });
}

async function file_robot() {
    try {
        let dataHead    = await read_dataFile('head.txt');
        let dataBody    = await read_dataFile('body.txt');
        let dataLeg     = await read_dataFile('leg.txt');
        let dataFeet    = await read_dataFile('feet.txt');
        
        let robotData = dataHead + os.EOL + dataBody + os.EOL + dataLeg + os.EOL + dataFeet;

        await write_dataFile('robot.txt', robotData);

        console.log('All completed!');
    } catch (error) {
        console.error(error);
    }
}

file_robot();
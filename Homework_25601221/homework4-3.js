/*
• อ่าน JSON จากไฟล์ homework1.json จากการบ้านวันที่ 1 ด้วย fs.readFile() และ JSON.Parse() เพื่อน า
ขอ้ มูลเขา้ มาอ่าน
• สร้าง JSON Array ที่คดั เอาเฉพาะพนกั งานที่มีเงินเดือนนอ้ ยกวา่ 100,000 ออกมา และเพิ่มเงินเดือนให้
พนกั งานเหล่าน้นั 2 เท่า
• หาผลรวมของเงินเดือนที่ตอ้ งจ่ายในแต่ละเดือนท้งั หมดหลงั ข้ึนเงินเดือน โดยรวมพนกั งานที่ไม่ไดข้ ้ึน
เงินเดือนด้วย (คาใบค้ ือไม่ตอ้ งหาทางรวมกอ้ น JSON Array กอ้ นเก่ากบั ใหม่กนั กไ็ ด้ จงใชส้ ิ่งที่มีอยแู่ ลว้ ใน
เรื่อง Pointer ใหเ้ กิดประโยชน์)
• ห้ามใช้ loop for, while ในโจทยข์ อ้ น้ีเดด็ ขาด ใชไ้ ดแ้ ค่ map, reduce, filter เท่าน้นั
• สร้างไฟล์ชื่อ homework4-3.js
*/

/*
array.map(function(currentValue, index, arr), thisValue)
array.filter(function(currentValue, index, arr), thisValue)
array.reduce(function(total, currentValue, currentIndex, arr), initialValue)
*/

const fs = require('fs');

function read_dataFile(Filename) {
    return new Promise(function(resolve, reject) {
        fs.readFile(Filename, 'utf8', function(err, dataFile) {
            if (err)
                reject(err);
            else
                resolve(dataFile);
        });
    });
}

async function create_Data() {
    try {
        const JSON_Data = await read_dataFile('homework4-3.json');
        const employeeData = JSON.parse(JSON_Data);

        //ปรับเงินเดือน
        function set_filterData_salary(currentValue_employeeData, index_employeeData, arr_employeeData) {
            return (currentValue_employeeData.salary < 1e5);
        }

        function set_mapData_salary(currentValue_employeeDataNew, index_employeeDataNew, arr_employeeDataNew) {
            currentValue_employeeDataNew.salary = currentValue_employeeDataNew.salary * 2;

            return currentValue_employeeDataNew;;
        }

        const employeeDataNew = employeeData
        .filter(set_filterData_salary)
        .map(set_mapData_salary);

        //หาผลรวมเงินเดือน
        function set_reduceData_salary(total_employeeData, currentValue_employeeData, index_employeeData, arr_employeeData) {
            return (total_employeeData + Number(currentValue_employeeData.salary));
        }

        let sumSalary = employeeData.reduce(set_reduceData_salary,0);

        sumSalary.toLocaleString('en')

        console.log(employeeData);
        console.log('ผลรวมเงินเดือน : ', sumSalary.toLocaleString());

        console.log('All completed!');
    } catch (error) {
        console.error(error);
    }
}

create_Data();
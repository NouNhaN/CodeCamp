/*
• อ่าน JSON จากไฟล์ homework1.json จากการบ้านวันที่ 1 ด้วย fs.readFile() และ JSON.Parse() เพื่อน า
ขอ้ มูลเขา้ มาอ่าน
• สร้าง JSON Array กอ้ นใหม่ที่เพิ่ม field ใหม่ชื่อ fullname ข้ึนมา โดยเกิดจากการนา string firstname และ
lastname มาต่อกนั โดย field อื่นๆ ยงั คงอยเหมือนเดิม ู่ แล้วแกไ้ ข salary จากเดิมเป็ น Number เปลี่ยนเป็ น
Array เพื่อใชป้ รับฐานเงินเดือนเพิ่มข้ึนในอีก 3 ปี โดย ใหเ้ งินเดือนข้ึนปี ละ 10% เช่น [20000,22000,
24200] โดยท าให้ครบทุกคน (ส่วนของ salary ห้าม for แต่ใหถ้ ึกป้อนค่า 3 ปี ตรงๆ เอา)
• ห้ามใช้ loop for, while ในโจทยข์ อ้ น้ีเดด็ ขาด ใชไ้ ดแ้ ค่ map, reduce, filter เท่าน้นั
• สร้างไฟล์ชื่อ homework4-2.js
*/

/*
var new_array = arr.map(function callback(currentValue[, index[, array]]) {
    // Return element for new_array
}[, thisArg])
*/

const fs = require('fs');

function read_dataFile(Filename) {
    return new Promise(function(resolve, reject) {
        fs.readFile(Filename, 'utf8', function(err, dataFile) {
            if (err)
                reject(err);
            else
                resolve(dataFile);
        });
    });
}

async function create_Data() {
    try {
        const JSON_Data = await read_dataFile('homework4-2.json');
        const employeeData = JSON.parse(JSON_Data);

        function set_employeeDataNew(currentValue_employeeData, index_employeeData, arr_employeeData) {
            let obj_employeeDataNew = {...currentValue_employeeData};

            obj_employeeDataNew['fullname'] = currentValue_employeeData.firstname + ' ' + currentValue_employeeData.lastname;

            let employeeData_salary = [];
            let salaryCal = Number(currentValue_employeeData.salary); 
            salaryCal = salaryCal + ((salaryCal * 10) / 100);   
            employeeData_salary.push(salaryCal);
            salaryCal = salaryCal + ((salaryCal * 10) / 100);   
            employeeData_salary.push(salaryCal);
            salaryCal = salaryCal + ((salaryCal * 10) / 100);   
            employeeData_salary.push(salaryCal);
            obj_employeeDataNew['salary'] = employeeData_salary;

            return obj_employeeDataNew;;
        }

        const employeeDataNew = employeeData.map(set_employeeDataNew);

        console.log(employeeData);
        console.log(employeeDataNew);
        console.log('All completed!');
    } catch (error) {
        console.error(error);
    }
}

create_Data();
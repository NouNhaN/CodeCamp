/*
• จงเข้า Download file
https://drive.google.com/open?id=13ejCslsXKkLx9XUTMRmxqOhMGOtNlcHS
• ใช้ JSON.Parse() อ่านค่าจาก file มาเกบ็ ค่าไว้
• จงสร้าง JSON Array ตวั ใหม่ที่มีเพียงเพศชาย, มีเพื่อนอยา่ งนอ้ ย 2 คน และกรอง field ออก โดยให้เหลือ
เพียง field name, gender, company, email, friends, balance เท่าน้นั
• ลดเงินในบัญชี (balance) ของทุกคนลง 10 เท่า (อยา่ ลืมเติม $ กลับที่ด้านหน้าด้วย)
• ห้ามใช้ loop for, while ในโจทยข์ อ้ น้ีเดด็ ขาด ใชไ้ ดแ้ ค่ map, reduce, filter เท่าน้นั
• เซพชื่อ homework4-4.js
*/

const fs = require('fs');

function read_dataFile(Filename) {
    return new Promise(function(resolve, reject) {
        fs.readFile(Filename, 'utf8', function(err, dataFile) {
            if (err)
                reject(err);
            else
                resolve(dataFile);
        });
    });
}

async function create_Data() {
    try {
        const JSON_Data = await read_dataFile('homework4-4.json');
        const employeeData = JSON.parse(JSON_Data);

        function set_filterData(currentValue_employeeData, index_employeeData, arr_employeeData) {
            return (currentValue_employeeData.gender === 'male') 
                    && (currentValue_employeeData.friends.length >= 2);
        }
        function set_mapData_employeeDataNew(currentValue_employeeData, index_employeeData, arr_employeeData) {
            let obj_employeeDataNew = {};

            obj_employeeDataNew['name'] = currentValue_employeeData.name;
            obj_employeeDataNew['gender'] = currentValue_employeeData.gender;
            obj_employeeDataNew['company'] = currentValue_employeeData.company;
            obj_employeeDataNew['email'] = currentValue_employeeData.email;
            obj_employeeDataNew['friends'] = currentValue_employeeData.friends;

            let employeeDataNew_balance = parseFloat(currentValue_employeeData.balance.slice(1).replace(/,/g, '')); 
            employeeDataNew_balance = employeeDataNew_balance * (90 / 100);
            obj_employeeDataNew['balance'] = '$' + employeeDataNew_balance.toLocaleString('en');

            return obj_employeeDataNew;;
        }

        const employeeDataNew = employeeData
        .filter(set_filterData)
        .map(set_mapData_employeeDataNew);    

        console.log(employeeData);
        console.log(employeeDataNew);

        console.log('All completed!');
    } catch (error) {
        console.error(error);
    }
}

create_Data();
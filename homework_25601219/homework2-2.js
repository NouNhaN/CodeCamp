let fields = ['id','firstname','lastname','company','salary'];
let employees = [
    ['1001','Luke','Skywalker','Walt Disney','40000'],
    ['1002','Tony','Stark','Marvel','1000000'],
    ['1003','Somchai','Jaidee','Love2work','20000'],
    ['1004','Monkey D','Luffee','One Piece','9000000']
];

let employeesData = [];

for (let i = 0; i < employees.length; i++) {

    let employeesRecord = {};

    for (let j  =0; j < employees[i].length; j++) {
        employeesRecord[fields[j]] = employees[i][j];
    }     

    employeesData.push(employeesRecord);
}    
console.log(employeesData); 
